package eu.specsproject.app.model;

import java.util.List;

public class CallResponse {
    private String status;
    private String data;
    private List<String> urls;

    public CallResponse(String status, String data){
        this.status = status;
        this.data = data;
    }
    
    public CallResponse(String status, String data, List<String> urls){
        this.status = status;
        this.data = data;
        this.urls = urls;
    }
    
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
    public List<String> getUrls() {
        return urls;
    }
    public void setUrls(List<String> urls) {
        this.urls = urls;
    }
    
}
