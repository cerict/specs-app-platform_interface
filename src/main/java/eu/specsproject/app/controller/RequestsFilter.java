package eu.specsproject.app.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.web.servlet.ModelAndView;


import eu.specsproject.app.utility.Common;
import eu.specsproject.app.utility.PropertiesManager;

@Scope("session")
public class RequestsFilter implements Filter {

	ArrayList<String> protectedPages;
	
	@Override
	public void destroy() {
		// ...
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		protectedPages = new ArrayList<String>();
		protectedPages.add("owner_home.jsp");
		protectedPages.add("platform_control_enforcement.jsp");
		protectedPages.add("platform_control_monitoring_dashboard.jsp");
		protectedPages.add("platform_control_monitoring.jsp");
		protectedPages.add("platform_control_negotiation.jsp");
		protectedPages.add("platform_control_platform.jsp");
		protectedPages.add("services_management.jsp");
		protectedPages.add("slas_management.jsp");
	}

	@Override
	public void doFilter(ServletRequest request, 
			ServletResponse response, FilterChain chain)
					throws IOException, ServletException {
//		System.out.println("doFilter CALLED: "+((HttpServletRequest)request).getRequestURL());
		
		
		/*String[] segments = ((HttpServletRequest)request).getRequestURL().toString().split("/");
		String lastpath = segments[segments.length-1];
		if(protectedPages.contains(lastpath)){
			HttpSession session = ((HttpServletRequest)request).getSession(false);
            if (null == session) {
            	request.setAttribute("error", "Session is null");
				request.setAttribute("error_description", "No session is found, try to execute the login!");
            	request.getRequestDispatcher("/redirect_error.jsp").forward(request, response);
            }else{
            	if(session.getAttribute("token") != null){
    				//Request access
    				String req_uri=((HttpServletRequest)request).getRequestURI();
    				HttpURLConnection httpURLConnection = doGetAccessRequest(
    						PropertiesManager.getProperty("access_verify.endpoint")+"?path_res="+req_uri+"&action="+Common.ACTION, session.getAttribute("token").toString());
    				String access_response = parseAccessResponse(httpURLConnection);
    				if(access_response.equals("PERMIT")){
    					chain.doFilter(request, response);
    				}else{
    					request.setAttribute("error", "Forbidden");
        				request.setAttribute("error_description", "The requested resource is not accessible! You are not authorized!");
    					request.getRequestDispatcher("/redirect_error.jsp").forward(request, response);
    				}
    			}else{
    				request.setAttribute("error", "Token is null");
    				request.setAttribute("error_description", "No token is found, try to execute the login!");
    				request.getRequestDispatcher("/redirect_error.jsp").forward(request, response);
    			}
            }
		}else{
			try {
				chain.doFilter(request, response);
			} catch (Exception ex) {
				System.out.println("Error");
				ex.printStackTrace();
				request.setAttribute("error", ex.getCause());
				request.setAttribute("error_description", ex.getMessage());
				request.getRequestDispatcher("/redirect_error.jsp")
				.forward(request, response);
			}
		}*/
		
		chain.doFilter(request, response);


	}
	
	private HttpURLConnection doGetAccessRequest(String urlPath, String token) throws IOException {
		URL url = new URL(urlPath);
		URLConnection c = url.openConnection();

		if (c instanceof HttpURLConnection) {
			HttpURLConnection httpURLConnection = (HttpURLConnection)c;
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setAllowUserInteraction(false);
			httpURLConnection.setRequestProperty("Authorization", "bearer "+token);
			httpURLConnection.connect();

			return httpURLConnection;
		}
		return null;
	}
	
	public static String parseAccessResponse(HttpURLConnection httpURLConnection) throws IOException{
		int responseCode = httpURLConnection.getResponseCode();
//		System.out.println("RESPONSE CODE: "+responseCode);
		if(responseCode == 200){
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String access_response = sb.toString();

//			System.out.println("ACCESS RESPONSE: "+access_response);
			return access_response;
		}else{
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String access_response = sb.toString();

//			System.out.println("ACCESS RESPONSE: "+access_response);
			return access_response;
		}
	}

}