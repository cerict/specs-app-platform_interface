<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SLA View</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel='stylesheet'
	href='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.css'
	type='text/css' media='all' />
<link rel='stylesheet' href='bootstrap/css/my-loading-bar.css'
	type='text/css' media='all' />

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}

.space {
	padding: 10px;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-SLAS-MANAGEMENT">

	<jsp:include page="utils/navigator_owner.jsp">
		<jsp:param name="_img_path" value="src='img/specs-logo-32.png'" />
		<jsp:param name="_slas_management" value="class='active'" />
	</jsp:include>

	<div class='container' ng-controller="slasManagementController">

		<div class="page-header">
			<h1>SLA Quick View</h1>
		</div>

		<div class="tab-pane" id="tabStoreParams">
			<p>Click on each category to get more details</p>

			<!-- API CALL -->

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#collapse0">Context</a> <span
							class="label label-primary label-as-badge">1</span>
					</h4>
				</div>
				<div id="collapse0" class="panel-collapse collapse">
					<div class="panel-body">

						<table class="table table-striped " style="width: 100%">
							<thead>
								<tr>
									<td style="width: 20%;"><b>AgreementInitiator</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;"><b>AgreementResponder</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;"><b>ServiceProvider</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;"><b>ExpirationTime</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;"><b>TemplateName</b></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 20%;">{{Contest.AgreementInitiator}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{Contest.AgreementResponder}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{Contest.ServiceProvider}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{Contest.ExpirationTime}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{Contest.TemplateName}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>


			<!--  SERVICE DESCRIPTION TERM - CONTROL FRAMEWORK E SECURITY CONTROL  -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#collapse3">Service
							Description Term</a> <span class="label label-primary label-as-badge">{{controlFrameworkSize+1}}</span>
					</h4>
				</div>
				<div id="collapse3" class="panel-collapse collapse">
					<div class="panel-body ">

						<table class="table table-striped " style="width: 100%">
							<thead>
								<tr>
									<td style="width: 20%;"><b>Control Framework</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 85%;"><b>Name</b></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 20%;">{{controlFramework.id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 85%;">{{controlFramework.frameworkName}}</td>
								</tr>
							</tbody>
						</table>

						<br />
						<table class="table table-striped" style="width: 100%;">
							<thead>
								<tr>
									<td style="width: 20%;"><b>Control Family</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 10%;"><b>ID</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 70%;"><b>Name</b></td>
								</tr>
							</thead>
							<tbody>
								<tr ng-if="isArray((controlFramework.NISTsecurityControl))"
									ng-repeat="item in controlFramework.NISTsecurityControl | orderBy:'+control_family'">

									<td style="width: 20%;">{{item.control_family}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 10%;">{{item.id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 70%;">{{item.name}}</td>
								</tr>
								<tr ng-if="!isArray(controlFramework.NISTsecurityControl)">

									<td style="width: 20%;">{{controlFramework.NISTsecurityControl.control_family}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 10%;">{{controlFramework.NISTsecurityControl.id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 70%;">{{controlFramework.NISTsecurityControl.name}}</td>
								</tr>


								<!--  CCM  
								<tr ng-if="Array.isArray(controlFramework.CCMsecurityControl)"
									ng-repeat="item in controlFramework.CCMsecurityControl">

									<td style="width: 20%;">{{item.control_family}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 10%;">{{item.id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 70%;">{{item.name}}</td>
								</tr>
								<tr ng-if="controlFramework.CCMsecurityControl && !Array.isArray(controlFramework.CCMsecurityControl)">
															  
									<td style="width: 20%;">{{controlFramework.CCMsecurityControl.control_family}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 10%;">{{controlFramework.CCMsecurityControl.id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 70%;">{{controlFramework.CCMsecurityControl.name}}</td>
								</tr>
								-->
							</tbody>
						</table>

					</div>
				</div>

			</div>


			<!--  SERVICE LEVEL OBJECTIVES -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#collapse2">Service
							Level Objectives</a> <span class="label label-primary label-as-badge">{{sloListSize}}</span>
					</h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse">
					<div class="panel-body ">


						<table class="table table-striped" style="width: 100%;">
							<thead>
								<tr>
									<td style="width: 10%;"><b>ID</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 50%;"><b>Metric Ref</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;"><b>Operator</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;"><b>Operand</b></td>
								</tr>
							</thead>
							<tbody>
								<tr ng-if="isArray(sloList.SLO)" ng-repeat="item in slos">

									<td style="width: 10%;">{{item.SLO_ID}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 50%;">{{item.MetricREF}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{item.SLOexpression.oneOpExpression.operator}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{item.SLOexpression.oneOpExpression.operand}}</td>
								</tr>
								<tr ng-if="!isArray(sloList.SLO)">

									<td style="width: 10%;">{{sloList.SLO.SLO_ID}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 50%;">{{sloList.SLO.MetricREF}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{sloList.SLO.SLOexpression.oneOpExpression.operator}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{sloList.SLO.SLOexpression.oneOpExpression.operand}}</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>

			</div>


		</div>
	</div>


	<jsp:include page="utils/footer.jsp" />

	<!-- JAVASCRIPT / ANGULAR  -->
	<script src="js/angular.js"></script>
	<script src="js/jquery.xml2json.js"></script>
	<script src="js/angular-animate.js"></script>

	<script src="js/XMLDisplay.js"></script>
	<script
		src='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.js'></script>

	<script>
		var app = angular.module('SPECS-SLAS-MANAGEMENT', [
				'angular-loading-bar', 'ngAnimate' ]);

		app.config([ 'cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
			cfpLoadingBarProvider.includeSpinner = false;
		} ]);

		app
				.controller(
						'slasManagementController',
						[
								'$scope',
								'$http',
								'$filter',
								'$location',
								'cfpLoadingBar',
								function($scope, $http, $filter, $location,
										cfpLoadingBar) {
									console.log("angular init");
									$scope.slaId = $location.search().slaId;
									console.log("Sla id received is: "
											+ $scope.slaId);

									$http(
											{
												method : 'GET',
												url : 'services/rest/viewSla?slaId='
														+ $scope.slaId
											})
											.then(
													function successCallback(
															response) {
														//console.log("XML" + response.data);
														$scope.SlaXml = response.data;

														$scope.SlaJson = $
																.xml2json($
																		.parseXML(response.data));
														console
																.log("JSON:"
																		+ $scope.SlaJson);
														getContest($scope.SlaJson);
														console
																.log("Context: "
																		+ $scope.Contest.AgreementInitiator
																		+ " - "
																		+ $scope.Contest.AgreementResponder
																		+ " "
																		+ $scope.Contest.ServiceProvider
																		+ " "
																		+ $scope.Contest.ExpirationTime
																		+ " "
																		+ $scope.Contest.TemplateName
																		+ "");

														getCapabilitiesAndControlFramework();
														
														if(Object.prototype.toString
																.call($scope.capabilities) === '[object Array]'){
															console
															.log("capabilities is an array");
														}
														else{
															if (Object.prototype.toString
																	.call($scope.capabilities.capability) === '[object Array]') {
																console
																		.log("capability is an array");
															} else {
																console
																		.log("\n***************************");
																console
																		.log("ControlFramework id: "
																				+ $scope.controlFramework.id);
																console
																		.log("ControlFramework name: "
																				+ $scope.controlFramework.frameworkName);
																console
																		.log("***************************");

																if (Object.prototype.toString
																		.call($scope.controlFramework.NISTsecurityControl) === '[object Array]') {
																	var controlFramweworkLenght = $scope.controlFramework.NISTsecurityControl.length;
																	console
																			.log("\n***************************");
																	for (var i = 0; i < controlFramweworkLenght; i++) {
																		console
																				.log("SecurityControl id: "
																						+ $scope.controlFramework.NISTsecurityControl[i].id);
																		console
																				.log("SecurityControl name: "
																						+ $scope.controlFramework.NISTsecurityControl[i].name);
																		console
																				.log("SecurityControl control_family: "
																						+ $scope.controlFramework.NISTsecurityControl[i].control_family);
																		console
																				.log("SecurityControl securityControl: "
																						+ $scope.controlFramework.NISTsecurityControl[i].securityControl);
																		console
																				.log("SecurityControl control_enhancement: "
																						+ $scope.controlFramework.NISTsecurityControl[i].control_enhancement);
																		console
																				.log("************************************");
																	}
																} else {
																	console
																			.log("SecurityControl id : "
																					+ $scope.controlFramework.NISTsecurityControl.id);
																	console
																			.log("SecurityControl name: "
																					+ $scope.controlFramework.NISTsecurityControl.name);
																	console
																			.log("SecurityControl control_family: "
																					+ $scope.controlFramework.NISTsecurityControl.control_family);
																	console
																			.log("SecurityControl securityControl: "
																					+ $scope.controlFramework.NISTsecurityControl.securityControl);
																	console
																			.log("SecurityControl control_enhancement: "
																					+ $scope.controlFramework.NISTsecurityControl.control_enhancement);
																}

																//for(var i=0; i<)
																//console.log("Capability : "+$scope.capabilities.capability.id);
																//console.log(" - id : "+$scope.capabilities.capability.name);
															}
														}

														

														getServiceLevelObjective();

														if (Object.prototype.toString
																.call($scope.sloList.SLO) === '[object Array]') {
															console
																	.log("sloList is an array");
															var sloLength = $scope.sloList.SLO.length;
															console
																	.log("sloLenght: "
																			+ sloLength
																			+ "\n\n\n");
															for (var i = 0; i < sloLength; i++) {
																console
																		.log("SLO id: "
																				+ $scope.sloList.SLO[i].SLO_ID);
																console
																		.log("SLO MetricREF: "
																				+ $scope.sloList.SLO[i].MetricREF);
																console
																		.log("SLO SLOexpression: "
																				+ $scope.sloList.SLO[i].SLOexpression.oneOpExpression.operator
																				+ " - "
																				+ $scope.sloList.SLO[i].SLOexpression.oneOpExpression.operand);
																console
																		.log("*********************");

															}
														} else {
															console
															.log("sloList is NOT an array");
															console
																	.log("SLO id: "
																			+ $scope.sloList.SLO.SLO_ID);
															console
																	.log("SLO MetricREF: "
																			+ $scope.sloList.SLO.MetricREF);
															console
																	.log("SLO SLOexpression: "
																			+ $scope.sloList.SLO.SLOexpression.oneOpExpression.operator
																			+ " - "
																			+ $scope.sloList.SLO.SLOexpression.oneOpExpression.operand);
														}

													},
													function errorCallback(
															response) {
														alert("Please contact system administrator!!!");

													});

									function getContest($value) {
										$scope.Contest = $value.Context;
									}

									function getCapabilitiesAndControlFramework() {
										$scope.capabilities = $scope.SlaJson.Terms.All.ServiceDescriptionTerm.serviceDescription.capabilities;
										if($scope.isArray($scope.capabilities.capability)){
											console.log("capabilities.capability e' array");
											$scope.controlFramework = $scope.capabilities.capability[0].controlFramework;
											$scope.controlFrameworkSize = $scope.isArray($scope.capabilities.capability[0].controlFramework.NISTsecurityControl) ? ($scope.controlFramework.NISTsecurityControl.length) : 1;

										}
										else{
											$scope.controlFramework = $scope.capabilities.capability.controlFramework;
											$scope.controlFrameworkSize = $scope.isArray($scope.capabilities.capability.controlFramework.NISTsecurityControl) ? ($scope.controlFramework.NISTsecurityControl.length) : 1;

										}

										
									}

									function getServiceLevelObjective() {
										console.log("getServiceLevelObjective");
										if($scope.isArray($scope.SlaJson.Terms.All.GuaranteeTerm)){
											$scope.sloList = $scope.SlaJson.Terms.All.GuaranteeTerm[0].ServiceLevelObjective.CustomServiceLevel.objectiveList;
										}
										else{
											$scope.sloList = $scope.SlaJson.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList;
										}
										
										$scope.slos = $scope.sloList.SLO;
										$scope.sloListSize = $scope.isArray($scope.sloList.SLO) ? $scope.sloList.SLO.length : 1;
										console.log("fine getServiceLevelObjective");
									}
									
									$scope.isArray = function(object) {
									    if(Object.prototype.toString
												.call(object) === '[object Array]') {
									       return true;
									    }
									    return false;
									}

								} ]);
	</script>
</body>
</html>