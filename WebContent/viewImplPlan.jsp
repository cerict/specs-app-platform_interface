<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SLA View</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel='stylesheet'
	href='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.css'
	type='text/css' media='all' />
<link rel='stylesheet' href='bootstrap/css/my-loading-bar.css'
	type='text/css' media='all' />

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}

.space {
	padding: 10px;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-ImplPlan-MANAGEMENT">

	<jsp:include page="utils/navigator_owner.jsp">
		<jsp:param name="_img_path" value="src='img/specs-logo-32.png'" />
		<jsp:param name="_viewImplPlan" value="class='active'" />
	</jsp:include>

	<div class='container' ng-controller="implementationPlanController">

		<div class="page-header">
			<h1>ImplementationPlan Quick View</h1>
		</div>

		<div class="tab-pane" id="tabStoreParams">
			<p>Choose an Implementation Plan from your disk</p>



			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">

						<input type="file" id="file-input" />
				</div>

			</div>


			<div class="panel panel-default" ng-show="ready">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#collapsePretty">Pretty
							Content</a>
					</h4>
				</div>
				<div id="collapsePretty" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table table-condensed " style="width: 100%">
							<thead>
								<tr>
									<td style="width: 20%;"><b>SLA Id</b></td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;"><b>Plan Id</b></td>
									<td style="width: 5%;"></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 20%;">{{plan.plan_id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 20%;">{{plan.sla_id}}</td>
									<td style="width: 5%;"></td>
								</tr>
							</tbody>

						</table>


						<div ng-repeat="item in plan.csps">

							
							<div class="panel panel-default" ng-show="ready">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse"
											data-target="#collapsePretty{{$index+1}}">CSP
											{{$index+1}}</a>
									</h4>
								</div>
								<div id="collapsePretty{{$index+1}}"
									class="panel-collapse collapse">
									<div class="panel-body">


										<table class="table table-condensed " style="width: 100%">

											<thead>
												<tr>
													<td style="width: 10%;"><b>VM #</b></td>
													<td style="width: 5%;"></td>
													<td style="width: 10%;"><b>IPs</b></td>
													<td style="width: 5%;"></td>
													<td style="width: 20%;"><b>Components</b></td>
													<td style="width: 5%;"></td>
													<td style="width: 50%;"><b>Details</b></td>
													<td style="width: 5%;"></td>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="vmItem in item.pools[0].vms">
													<td style="width: 10%;">{{$index+1}}</td>
													<td style="width: 5%;"></td>
													<td style="width: 10%;">
														<ul style="list-style-type: disc">
															<li>{{vmItem.public_ip}}</li>
														</ul>
													</td>
													<td style="width: 5%;"></td>
													<td style="width: 20%;">
														<ul style="list-style-type: disc">
															<li ng-repeat="component in vmItem.components">
																{{component.recipe}}</li>
														</ul>
													</td>
													<td style="width: 5%;"></td>
													<td style="width: 50%;">
														<ul style="list-style-type: disc">
															<li>Zone: {{item.iaas.zone}}</li>
															<li>Appliance: {{item.iaas.appliance}}</li>
															<li>Hardware: {{vmItem.hardware}}</li>
														</ul>
													</td>
													<td style="width: 5%;"></td>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default" ng-show="ready">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#collapseRaw">Raw
							Content</a>
					</h4>
				</div>
				<div id="collapseRaw" class="panel-collapse collapse">
					<div class="panel-body">
						<pre id="file-content"></pre>
					</div>
				</div>

			</div>

		</div>


		<jsp:include page="utils/footer.jsp" />

		<!-- JAVASCRIPT / ANGULAR  -->
		<script src="js/angular.js"></script>
		<script src="js/jquery.xml2json.js"></script>
		<script src="js/angular-animate.js"></script>

		<script src="js/XMLDisplay.js"></script>
		<script
			src='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.js'></script>

		<script>
			var app = angular.module('SPECS-ImplPlan-MANAGEMENT', [
					'angular-loading-bar', 'ngAnimate' ]);

			app.config([ 'cfpLoadingBarProvider',
					function(cfpLoadingBarProvider) {
						cfpLoadingBarProvider.includeSpinner = false;
					} ]);

			app
					.controller(
							'implementationPlanController',
							[
									'$scope',
									'$http',
									'$filter',
									'$location',
									'cfpLoadingBar',
									function($scope, $http, $filter, $location,
											cfpLoadingBar) {
										console.log("angular init");
										$scope.slaId = $location.search().slaId;

										$scope.ready = false;

										$scope.plan = {};

										function readSingleFile(e) {
											var file = e.target.files[0];
											if (!file) {
												return;
											}
											var reader = new FileReader();

											reader.readAsText(file);

											reader.onload = function(e) {
												var contents = e.target.result;
												$scope.ready = true;
												$scope.plan = angular
														.fromJson(contents);

												displayContents(contents);

												$scope.$apply();
											};

										}

										function displayContents(contents) {
											var element = document
													.getElementById('file-content');
											element.innerHTML = contents;
										}

										document.getElementById('file-input')
												.addEventListener('change',
														readSingleFile, false);

										function getContest($value) {
											$scope.Contest = $value.Context;
										}

										function getCapabilitiesAndControlFramework() {
											$scope.capabilities = $scope.SlaJson.Terms.All.ServiceDescriptionTerm.serviceDescription.capabilities;
											if ($scope
													.isArray($scope.capabilities.capability)) {
												console
														.log("capabilities.capability e' array");
												$scope.controlFramework = $scope.capabilities.capability[0].controlFramework;
												$scope.controlFrameworkSize = $scope
														.isArray($scope.capabilities.capability[0].controlFramework.NISTsecurityControl) ? ($scope.controlFramework.NISTsecurityControl.length)
														: 1;

											} else {
												$scope.controlFramework = $scope.capabilities.capability.controlFramework;
												$scope.controlFrameworkSize = $scope
														.isArray($scope.capabilities.capability.controlFramework.NISTsecurityControl) ? ($scope.controlFramework.NISTsecurityControl.length)
														: 1;

											}

										}

										function getServiceLevelObjective() {
											console
													.log("getServiceLevelObjective");
											if ($scope
													.isArray($scope.SlaJson.Terms.All.GuaranteeTerm)) {
												$scope.sloList = $scope.SlaJson.Terms.All.GuaranteeTerm[0].ServiceLevelObjective.CustomServiceLevel.objectiveList;
											} else {
												$scope.sloList = $scope.SlaJson.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList;
											}

											$scope.slos = $scope.sloList.SLO;
											$scope.sloListSize = $scope
													.isArray($scope.sloList.SLO) ? $scope.sloList.SLO.length
													: 1;
											console
													.log("fine getServiceLevelObjective");
										}

										$scope.isArray = function(object) {
											if (Object.prototype.toString
													.call(object) === '[object Array]') {
												return true;
											}
											return false;
										}

									} ]);
		</script>
</body>
</html>