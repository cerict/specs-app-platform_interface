<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS Services Management</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.list-group {
	max-height: 200px;
	min-height: 200px;
	overflow-y: scroll;
	vertical-align: text-top;
	background-color: #ffffff;
	border: 2px solid #428bca;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-SLAS-MANAGEMENT">

	<jsp:include page="utils/navigator_owner.jsp">
		<jsp:param name="_img_path" value="src='img/specs-logo-32.png'" />
		<jsp:param name="_services_management" value="class='active'" />
	</jsp:include>

	<div class='container' ng-controller="slasManagementController">

		<div class="page-header">
			<h1>SPECS Services management</h1>
		</div>

		<div class="tab-pane" id="tabStoreParams">

			<!-- API CALL -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Services Specification</h2>
				</div>
				<div class="panel-body ">

					<table style="width: 100%;">
						<tr style="width: 100%;">
							<td style="width: 32%;">
								<div class="well" style="width: 100%;">
									<b>Security Services</b>
								</div>
							</td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;">
								<div class="well" style="width: 100%;">
									<b>Security Mechanisms (Metadata)</b>
								</div>
							</td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;">
								<div class="well" style="width: 100%;">
									<b>Security Mechanisms (Artifacts)</b>
								</div>
							</td>
						</tr>


						<tr>
							<td style="width: 32%;">
								<div class="list-group">
									<a ng-repeat="serviceId in servicesId"
										ng-click="$parent.selectedService = serviceId"
										class="list-group-item"
										ng-class="{active:selectedService==serviceId}">{{serviceId}}</a>
								</div> <br />
							</td>
							<td style="width: 2%;"></td>

							<td style="width: 32%;">
								<div class="list-group">
									<a ng-repeat="securityMechanismId in securityMechanismsId"
										ng-click="$parent.selectedMechanism = securityMechanismId"
										class="list-group-item"
										ng-class="{active:selectedMechanism==securityMechanismId}">{{securityMechanismId}}</a>
								</div> <br />
							</td>

							<td style="width: 2%;"></td>
							<td style="width: 32%;">
								<div class="list-group">
									<a ng-repeat="recipeName in recipesName"
										ng-click="$parent.selectedRecipe = recipeName"
										class="list-group-item">{{recipeName}}</a>
								</div> <br />
							</td>
						</tr>

						<tr style="width: 100%;">
							<td style="width: 32%;"><input id="service-chooser"
								type="file" class="filestyle" accept="text/xml"><br />
							<td style="width: 2%;"></td>
							<td style="width: 32%;"><input
								id="security-machanisms-chooser" type="file" class="filestyle"
								accept="application/json"><br /></td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;"><br /></td>
						</tr>

						<tr style="width: 100%;">
							<td style="width: 32%;" align="center"><button
									style="width: 100%" class="btn btn-primary"
									onClick="addService()">Add Service</button> <br /> <br /></td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;" align="center"><button
									style="width: 100%" class="btn btn-primary"
									onClick="addSecurityMechanism()">Add Security
									Mechanism (Metadata)</button> <br /> <br /></td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;" align="center">
								<button style="width: 100%" class="btn btn-primary"
									ng-click="refreshArtifacts()">Refresh Security
									Mechanism (Artifact)</button> <br /> <br />
							</td>
						</tr>



					</table>
					<br />

					<table style="width: 100%;">
						<tr style="width: 100%;">
							<td style="width: 32%;" align="center"><br />
								<button style="width: 100%" class="btn btn-primary"
									ng-Click="checkCapabilities()">Check Capabilities</button></td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;" align="center"><br />
								<button style="width: 100%" class="btn btn-primary"
									ng-Click="checkArtifacts()">Check Artifacts</button></td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;" align="center"><br />
								<button style="width: 100%" class="btn btn-primary"
									ng-Click="checkUpdateArtifacts()">Check Update Status</button></td>
						</tr>

						<tr style="width: 100%;">
							<td style="width: 32%;" align="center"><br />
								<div class="list-group">
									<a ng-repeat="allCapability in allCapabilities"
										ng-click="$parent.selectedRecipe = allCapability.capabilityId"
										class="list-group-item {{allCapability.rowStyle}}"">{{allCapability.capabilityId}}
										- {{allCapability.mechanismsId}}</a>
								</div></td>
							<td style="width: 2%;"></td>

							<td style="width: 32%;" align="center"><br />
								<div class="list-group">
									<a ng-repeat="artifactCheck in artifactsCheck"
										ng-click="$parent.selectedRecipe = artifactCheck.name"
										class="list-group-item {{artifactCheck.rowStyle}}"">{{artifactCheck.name}}</a>
								</div></td>
							<td style="width: 2%;"></td>
							<td style="width: 32%;" align="center"><br /></td>
						</tr>
					</table>


				</div>

			</div>
		</div>
	</div>


	<jsp:include page="utils/footer.jsp" />

	<!-- JAVASCRIPT / ANGULAR  -->
	<script src="js/angular.js"></script>
	<script src="js/XMLDisplay.js"></script>
	<script src="js/jquery.xml2json.js"></script>
	<script src="bootstrap/js/bootstrap-filestyle.min.js"></script>

	<script>
		var app = angular.module('SPECS-SLAS-MANAGEMENT', []);

		app
				.controller(
						'slasManagementController',
						[
								'$scope',
								'$http',
								'$filter',
								function($scope, $http, $filter) {
									console.log("angular init");

									$scope.itemColor = "list-group-item-success";
									$scope.contentTypeEnabled = false;

									//GET SERVICES
									$http({
										method : 'GET',
										url : 'services/rest/getServices'
									})
											.then(
													function successCallback(
															response) {
														console
																.log(response.data);
														$scope.services = $
																.xml2json($
																		.parseXML(response.data.data)).itemList;
														$scope.servicesId = [];

														if ($scope.services instanceof Array) {
															for (i = 0; i < $scope.services.length; i++) {
																$scope.servicesId
																		.push($scope.services[i]
																				.split("/")[$scope.services[i]
																				.split("/").length - 1]);
															}
														} else {
															$scope.servicesId
																	.push($scope.services
																			.split("/")[$scope.services
																			.split("/").length - 1])
														}

														console
																.log($scope.services);
														console
																.log($scope.servicesId);
													},
													function errorCallback(
															response) {

													});

									//GET SECURITY MECHANISMS
									$http(
											{
												method : 'GET',
												url : 'services/rest/getSecurityMechanisms'
											})
											.then(
													function successCallback(
															response) {
														console
																.log(response.data);
														$scope.securityMechanisms = response.data.itemList;
														$scope.securityMechanismsId = [];

														if ($scope.securityMechanisms instanceof Array) {
															for (i = 0; i < $scope.securityMechanisms.length; i++) {
																$scope.securityMechanismsId
																		.push($scope.securityMechanisms[i]
																				.split("/")[$scope.securityMechanisms[i]
																				.split("/").length - 1]);
															}
														} else {
															$scope.securityMechanismsId
																	.push($scope.securityMechanisms
																			.split("/")[$scope.securityMechanisms
																			.split("/").length - 1]);

														}

														console
																.log($scope.securityMechanisms);
														console
																.log($scope.securityMechanismsId);
													},
													function errorCallback(
															response) {

													});

									//GET ARTIFACTS
									$http({
										method : 'GET',
										url : 'services/rest/getArtifacts'
									})
											.then(
													function successCallback(
															response) {
														console
																.log(response.data);
														$scope.cookJson = JSON
																.parse(response.data.data);
														$scope.cookbooks = Object
																.keys($scope.cookJson.cookbooks);

														$scope.recipesName = [];
														console
																.log($scope.cookbooks);
														if ($scope.cookbooks instanceof Array) {
															for (i = 0; i < $scope.cookbooks.length; i++) {
																console
																		.log($scope.cookJson.cookbooks[$scope.cookbooks[i]].recipes);
																$scope.recipes = $scope.cookJson.cookbooks[$scope.cookbooks[i]].recipes;

																for (j = 0; j < $scope.recipes.length; j++) {
																	$scope.recipesName
																			.push($scope.cookbooks[i]
																					+ ":"
																					+ $scope.recipes[j]);
																}

															}
														}

													},
													function errorCallback(
															response) {
														console
																.log(response.data);
													});

									//CHECK CAPABILITIES
									$scope.checkCapabilities = function() {
										console.log($scope.selectedService);
										$http(
												{
													method : 'GET',
													url : 'services/rest/checkCapabilities?service_id='
															+ $scope.selectedService
												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response.data);
															$scope.allCapabilities = response.data;

														},
														function errorCallback(
																response) {
															console
																	.log(response.data);

														});
									}

									//CHECK ARTIFACTS
									$scope.checkArtifacts = function() {
										console.log($scope.selectedService);
										$http(
												{
													method : 'GET',
													url : 'services/rest/checkArtifacts?mechanism_id='
															+ $scope.selectedMechanism
												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response.data);
															$scope.allArtifacts = response.data;
															$scope.artifactsCheck = [];
															for (i = 0; i < $scope.allArtifacts.length; i++) {
																var eachArtifact = {
																	"name" : $scope.allArtifacts[i],
																	"rowStyle" : 'list-group-item-danger'
																};
																//$scope.artifactsCheckOne.name = $scope.allArtifacts[i];
																//$scope.artifactsCheckOne.rowStyle = 'list-group-item-danger';
																for (j = 0; j < $scope.recipesName.length; j++) {
																	if ($scope.allArtifacts[i] == $scope.recipesName[j]) {
																		eachArtifact.rowStyle = 'list-group-item-success';
																	}
																}
																$scope.artifactsCheck
																		.push(eachArtifact);
															}
														},
														function errorCallback(
																response) {
															console
																	.log(response.data);

														});
									}

									//REFRESH ARTIFACTS
									$scope.refreshArtifacts = function() {
										$http(
												{
													method : 'GET',
													url : 'services/rest/updateChefServer'
												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response.data);
															alert("The Task Submission Status is: "+response.data.message);
														},
														function errorCallback(
																response) {
															console
																	.log(response.data);
															alert("Error during the request! Retry please\n\nDetail:\n"+response.data);

														});

									}
									
									//CHECK UPDATE STATUS ARTIFACTS
									$scope.checkUpdateArtifacts = function() {
										$http(
												{
													method : 'GET',
													url : 'services/rest/verifyChefServer'
												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response.data);
															alert("The Update Status is: "+response.data.message);
															if(response.data.message == "done"){
																window.location.reload();
															}
														},
														function errorCallback(
																response) {
															console
																	.log(response.data);
															alert("Error during the request! Retry please\n\nDetail:\n"+response.data);

														});

									}
									
								} ]);

		function addService() {
			var file = document.getElementById('service-chooser').files[0];
			console.log(file.name);
			console.log(file.size);

			// Create a new FormData object.
			var formData = new FormData();
			formData.append("datafile", file, file.name);

			var response = ajaxCall('services/rest/addSecurityService', 'POST',
					formData, null, false);
			console.log(response.status);

			if (response.status == 201) {
				alert(response.status + " - " + response.statusText + "\n"
						+ response.responseText + "\n\n"
						+ "The Security Service is correctly uploaded!");
				window.location.reload();
			} else {
				alert("Upload of security service unsuccessfully!\n Error: "
						+ response.status + " - " + response.statusText + "\n"
						+ response.responseText);
			}
		}

		function addSecurityMechanism() {
			var file = document.getElementById('security-machanisms-chooser').files[0];
			console.log(file.name);
			console.log(file.size);

			// Create a new FormData object.
			var formData = new FormData();
			formData.append("datafile", file, file.name);

			var response = ajaxCall('services/rest/addSecurityMechanism',
					'POST', formData, null, false);
			console.log(response.status);

			if (response.status == 201) {
				alert(response.status + " - " + response.statusText + "\n"
						+ response.responseText + "\n\n"
						+ "The Security Mechanism is correctly uploaded!");
				window.location.reload();
			} else {
				alert("Upload of security mechanism unsuccessfully!\n Error: "
						+ response.status + " - " + response.statusText + "\n"
						+ response.responseText);
			}
		}

		function ajaxCall(url, method, body, contenttype, asynch) {

			var xmlhttp;

			if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.open(method, url, asynch);

			if (contenttype != null)
				xmlhttp.setRequestHeader("Content-type", contenttype);

			xmlhttp.send(body);
			return xmlhttp;
		}
	</script>
</body>
</html>